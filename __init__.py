# todo support multiple layers
# use GPS coordinates
# async tile fetch (rendering lowers res instead)

import st3m.run, random, math
import urequests

from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from ctx import Context

import os

def file_exists(filename):
    try:
        os.stat(filename)
        return True
    except OSError:
        return False


full_width = 7081
full_height = 6543

lon_start = 13.3030804
lon_end = 13.3119122

lat_start = 53.0340536
lat_end = 53.0291095


def x_to_lon(val):
    return (x / full_width) * (lon_end - lon_start) + lon_start


def y_to_lat(val):
    return (y / full_height) * (lat_end - lat_start) + lat_start


def lon_to_x(val):
    return (val - lon_start) / (lon_end - lon_start) * full_width


def lat_to_y(val):
    return (val - lat_start) / (lat_end - lat_start) * full_height


class Map(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self.lon = 13.30673763
        self.lat = 53.03092808
        self.x = lon_to_x(self.lon)
        self.y = lat_to_y(self.lat)
        self.scale = 0.5
        self.tile_dim = 64
        self._tc = 4
        self._tch = 2
        self.scale_dir = 1
        self.x_dir = 1
        self.y_dir = 1

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        x_speed = 0.00
        y_speed = 0.00
        if ins.captouch.petals[1].position[0] > 20:
            x_speed = 0.03 * ins.captouch.petals[1].position[0] / 10000
            y_speed = -0.03 * ins.captouch.petals[1].position[0] / 10000
        if ins.captouch.petals[3].position[0] > 20:
            x_speed = 0.03 * ins.captouch.petals[3].position[0] / 10000
            y_speed = 0.005 * ins.captouch.petals[3].position[0] / 10000
        if ins.captouch.petals[5].position[0] > 20:
            x_speed = 0.00 * ins.captouch.petals[5].position[0] / 10000
            y_speed = 0.03 * ins.captouch.petals[5].position[0] / 10000
        if ins.captouch.petals[7].position[0] > 20:
            x_speed = -0.03 * ins.captouch.petals[7].position[0] / 10000
            y_speed = 0.005 * ins.captouch.petals[7].position[0] / 10000
        if ins.captouch.petals[9].position[0] > 20:
            x_speed = -0.03 * ins.captouch.petals[9].position[0] / 10000
            y_speed = -0.03 * ins.captouch.petals[9].position[0] / 10000

        zoom_speed = 1.003
        self.x += delta_ms * x_speed * self.x_dir / self.scale
        self.y += delta_ms * y_speed * self.y_dir / self.scale
        for i in range(delta_ms / 10):
            if self.scale_dir > 0:
                self.scale *= zoom_speed
                if self.scale > (1 / 2.4):
                    self.scale_dir = -1
            else:
                self.scale /= zoom_speed
                if self.scale < 0.01:
                    self.scale_dir = 1
        if (self.x >= full_width) or (self.x < 0):
            self.x -= delta_ms * x_speed * self.x_dir / self.scale
            self.x_dir = -1 * self.x_dir
        if (self.y >= full_height) or (self.y < 0):
            self.y -= delta_ms * y_speed * self.y_dir / self.scale
            self.y_dir = -1 * self.y_dir

    def draw(self, ctx: Context) -> None:
        tile_dim = self.tile_dim
        ctx.image_smoothing = False
        ctx.rectangle(-120, -120, 240, 240).rgb(0, 0.34, 0.72).fill()
        if self.scale > 1.0:
            z = 0
            scale = self.scale
        else:
            logscale = math.log(self.scale, 0.5)
            z = math.floor(logscale)
            scale = 2 ** (-(logscale - z))
        # we can not rely on transforms, the happy path for ctx.image is
        # working without any scale factor - sorry

        u = self.x / (2 ** (z)) / 64
        v = self.y / (2 ** (z)) / 64

        u_int = math.floor(u)
        v_int = math.floor(v)

        u_fraction = u - u_int
        v_fraction = v - v_int

        scale *= 2.3
        tiles_folder = "/sd/map/"

        for col in range(self._tc):
            for row in range(self._tc):
                if (u_int + col - self._tch) >= 0 and (v_int + row - self._tch) >= 0:
                    tile_path_fragment = (
                        str(u_int + col - self._tch)
                        + "/"
                        + str(v_int + row - self._tch)
                        + "/"
                        + str(z)
                        + ".png"
                    )
                    tile_path = tiles_folder + tile_path_fragment
                    try:
                        os.stat(tile_path)
                    except OSError:
                        response = urequests.get("http://pippin.gimp.org/tmp/flow3r-map/" + tile_path_fragment)

                        try:
                            os.mkdir(tiles_folder)
                        except OSError:
                            True
                        try:
                            os.mkdir(tiles_folder + str(u_int + col - self._tch))
                        except OSError:
                            True
                        try:
                            os.mkdir(
                                tiles_folder
                                + str(u_int + col - self._tch)
                                + "/"
                                + str(v_int + row - self._tch)
                            )
                        except OSError:
                            True
                        print(
                            tiles_folder
                            + str(u_int + col - self._tch)
                            + "/"
                            + str(v_int + row - self._tch)
                        )
                        print(tile_path)
                        f = open(tile_path, "w")
                        f.write(response.content)
                        f.close()

                    ctx.image(
                        tile_path,
                        ((col - self._tch - u_fraction) * tile_dim) * scale,
                        ((row - self._tch - v_fraction) * tile_dim) * scale,
                        tile_dim * scale + 1,
                        tile_dim * scale + 1,
                    )


if __name__ == "__main__":
    st3m.run.run_view(Map(ApplicationContext()))

