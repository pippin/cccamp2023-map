#!/bin/bash

#this is a shell script for turning a giant image into a tiled mipmap pyramid

EXT=png
dim=64
input=full.png
input_width=`file full.png | cut -f 2 -d ',' | cut -f 1 -d x`
input_height=`file full.png | cut -f 2 -d ',' | cut -f 1 -d x`

max_x=$((input_width/dim))
max_y=$((input_height/dim))

scale=1.0
for z in {0..7};do

for x in $(seq 62 $(($max_x+1)));do
  u=$(($x * $dim));
  gegl full.png -o col0.png -- scale-ratio x=$scale y=$scale crop x=$u y=0 width=$dim height=7000;
  for y in $(seq 0 $(($max_y+1)));do
    v=$(($y * $dim));
    mkdir -p tiles/$dim/$x/$y/
    echo gegl col0.png -o tiles/$dim/$x/$y/$z.$EXT -- crop x=0 y=$v width=$dim height=$dim;
    gegl col0.png -- crop x=0 y=$v width=$dim height=$dim png-save path=tiles/$dim/$x/$y/$z.$EXT bitdepth=8;
done
done

  scale=$(echo "scale=10;$scale/2" | bc -l)
  max_x=$((max_x/2))
  max_y=$((max_y/2))
done
exit
